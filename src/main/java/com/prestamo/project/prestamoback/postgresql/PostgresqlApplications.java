package com.prestamo.project.prestamoback.postgresql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostgresqlApplications {
	
	public static void main(String[] args) {
		SpringApplication.run(PostgresqlApplications.class, args);
	}


}
