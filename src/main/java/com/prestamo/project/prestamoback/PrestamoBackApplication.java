package com.prestamo.project.prestamoback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrestamoBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrestamoBackApplication.class, args);
	}

}
